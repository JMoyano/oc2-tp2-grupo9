section .text 

global enmascarar_asm
enmascarar_asm:
    push ebp ;enter
    mov ebp, esp  ;enter
    
    xor ebx,ebx       ;lo limpio para que ebx actue como contador en el ciclo
    mov edx,[EBP+20] ;cantidad bytes imagenes
   	 
cicloSIMD:
    ;valido si edx (cantidad de bytes) es menor a 8 
    cmp edx,8
    jl cicloMenorA8
    
    mov eax,[ebp+8]    ;cargo la img1 en eax
    MOVQ mm0,[eax+ebx] ;cargo los 8 bytes de la img1 en mm0, uso ebx para desplazarme
    
    mov eax,[ebp+12]   ;cargo la img2 en eax
    MOVQ mm1,[eax+ebx] ;cargo los 8 bytes de la img2 en mm1, uso ebx para desplazarme
    
    mov eax,[ebp+16]   ;cargo la mascara en eax
    MOVQ mm2,[eax+ebx] ;cargo los 8 bytes de la mascara en mm2, uso ebx para desplazarme
   
    PAND mm1, mm2 ;mantiene los bytes de la img2 cuando los bytes de la mascara son 255 (blanco - FF)
    
    PANDN mm2, mm0 ;remplaza sobre los bytes de la mascara que estan en 0 (negro) y pone los bytes de la img1
    
    POR mm1, mm2 ;fusiona ambos resultados en mm1
    
    mov eax,[ebp+8] ; vuelvo a cargar img1 en eax para guardar cambios
    MOVQ qword[eax+ebx], mm1 ;guardo lo obtenido en mm1 en la img1, con su desplazamiento en ebx 
    
    add ebx,8 ;aumento 8 al desplazamiento del arreglo
    sub edx,8 ;resto 8 a la cantidad de bytes de las imagenes 
    
    jmp cicloSIMD ;repito el ciclo
    
cicloMenorA8:
    ;valido si edx es 0 para terminar en caso que la cantidad sea multiplo de 8 o haya terminado con los bytes extras
    cmp edx,0    
    je finCiclos
    
    mov eax,[ebp+16]   ;cargo la mascara en eax
    movzx ecx,byte[eax+ebx] ;tomo solamente el byte de la mascara
    
    
    cmp ecx,255     ;valido que el byte de la mascara sea 255 (blanco) y entro en la funcion para acoplar a la img1 el byte correspondiente a la img2
    je valorMascaraBlanco
    
    add ebx,1 ;aumento 1 al desplazamiento del arreglo
    sub edx,1 ;resto 1 a la cantidad de bytes de las imagenes
    
    jmp cicloMenorA8

valorMascaraBlanco:
    
    mov eax,[ebp+12]   ;cargo la img2 en eax
    movzx ecx,byte[eax+ebx] ;tomo solamente el byte que necesito agregar a la img1
    
    mov eax,[ebp+8]
    mov dword[eax+ebx],ecx ;agrego el byte de la img2 a la img1
           
    add ebx,1 ;aumento 1 al desplazamiento del arreglo
    sub edx,1 ;resto 1 a la cantidad de bytes de las imagenes
    
    jmp cicloMenorA8 ;vuelvo al ciclo de menorA8
               
finCiclos:
    mov ebp,esp ;leave
    pop ebp ;leave
    
    ret
