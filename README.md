# _Trabajo Practico N°2 - Organizacion del Computador 2 - 2do Cuatrimestre 2021_

## Enmascarar con SIMD

### Consigna:

Con el objetivo de comparar la performance de las instrucciones SIMD con las instrucciones
aritméticas y lógicas de la ALU, se pide desarrollar una aplicación de línea de comandos que combine
dos imágenes usando una máscara de selección. Implementaremos dos versiones de esta operación:
primero en lenguaje C y luego utilizando instrucciones SIMD en lenguaje ensamblador

Se debe implementar la función enmascarar_asm en lenguaje ensamblador de 32
bits usando instrucciones SIMD y respetando la convención de C de pasaje de parámetros. Al igual
que enmascarar_c en el lenguaje C. Ambas funciones deben pisar el contenido del buffer a, con el
resultado de combinar a y b usando la máscara mask.
Además se debe implementar un programa en C que reciba los parámetros mencionados por
línea de comandos y que llame a las dos funciones para generar dos archivos

- Esta consigna fue resuelta en los siguientes archivos 
[enmascarar.c](/enmascarar.c) y [enmascarar_asm.asm](/enmascarar_asm.asm)

### Script ejecucion:

Para ejecutar las funciones y realizar pruebas de distintos modelos de imagenes se genero un archivo [scriptEjecucion.sh](/scriptEjecucion.sh) el cual se ejecuta con la siguiente linea: 
`sh scriptEjecucion.sh` en una consola linux.

A continuacion se muestra un ejemplo de ejecucion:

![ejecucionScript](/imagenes/ejecucion/ejecucionScript.png)

Dentro del archivo **scriptEjecucion.sh** se encuentran los siguientes comandos. 

![datosScript](/imagenes/ejecucion/datosScript.png)

- El comando `nasm -f elf32 ...` realiza la compilacion del archivo .asm en un archivo del tipo .o. 
- El comando `gcc -m32 ...` se encarga de linkear el archivo .o que se genero anteriormente, con el archivo .c y generar un ejecutable. 
- El comando `./main` realiza la ejecucion del archivo ejecutable generado en la linea anterior con distintos parametros para las pruebas.

### Ejemplo ejecucion:

Se utilizaron las siguientes imagenes para una de las pruebas, con resolucion 1920x1080

##### Imagen 1
![img1](/imagenes/ejemploResultado/img1.jpg)

##### Imagen 2
![img2](/imagenes/ejemploResultado/img2.jpg)

##### Mascara
![mascara](/imagenes/ejemploResultado/mascara.jpg)

Cuando ejecutamos el comando:

```
./main 1920x1080/img1.bmp 1920x1080/img2.bmp 1920x1080/mascara.bmp 1920 1080
```

Obtenemos el siguiente resultado de C

![salida_c](/imagenes/ejemploResultado/salida_c.jpg)

### Otros resultados:

Otros ejemplos de resultados son los siguientes

##### 640x480 (Resultado de C)
![salida_c640x480](/imagenes/ejemploResultado/salida_c640x480.jpg)

##### 1291x902 (Resultado de ASM)
![salida_asm1291x902](/imagenes/ejemploResultado/salida_asm1291x902.jpg)

### Grafico comparativo:
![grafico](https://gitlab.com/JMoyano/oc2-tp2-grupo9/-/raw/master/imagenes/Grafico.png)

En este grafico podemos observar la comparativa entre los tiempos de ejecucion en milisegudos de la misma funcion tanto en _c_ como en _assembler_ para distintos tamaños de imagen. Puede verse claramente que la segunda no solo es mas rapida siempre, sino que la diferencia va haciendose mas grande a medida que los tamaños de imagen a procesar crecen, por lo que podemos inferir que la diferencia entre ambas crece exponencialmente a medida que aumentan las dimensiones del archivo a procesar.

### Conclusion:
Analizando los resultados obtenidos, podemos concluir que aprovechando las instrucciones SIMD mediante la funcion en lenguaje ensamblador, obtenemos un mejor rendimiento en el tiempo de ejecucion que al realizar la misma operacion en una funcion programada en _c_. Esto sucede porque en lugar de operar en la ALU pixel a pixel, mediante el conjunto de instrucciones SIMD podemos tomar de a grupos de pixeles y operar sobre ellos de manera paralela, impactando considerablemente en la performance de la funcion. Teniendo esto en cuenta, podemos asegurar que el set de instrucciones SIMD es mas performante a la hora de procesar imagenes y graficos. 

#### Integrantes grupo 9:
- _**Macias, Agustin Eduardo**_ :computer: - [agustinmacias](https://gitlab.com/agustinmacias)

- _**Moyano, Juan Gabriel**_ :computer: - [JMoyano](https://gitlab.com/JMoyano)

