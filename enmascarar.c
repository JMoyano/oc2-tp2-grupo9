#include<stdio.h>																																																												
#include<stdlib.h>
#include<string.h>
#include <sys/time.h>

extern void enmascarar_asm(unsigned char *a, unsigned char *b,unsigned char *mask, int cant);

void enmascarar_c(unsigned char *a, unsigned char *b, unsigned char *mask, int cant){
  int i;
  for(i=0;i<cant;i++){
      if (mask[i]!=0x00){
   	a[i]=b[i];        
      }
  }          
}

void abrirArchivo(char *archivo, int cantidad, unsigned char *buffer){
    FILE *A;
    A = fopen(archivo,"rb"); 
    if (A == NULL){
        printf("Error al abrir el archivo\n");
    }else{
    	fread(buffer, cantidad, 1,A); 
    }
    fclose(A);
}

void guardarArchivo(char *archivo, int cantidad, unsigned char *buffer){
    FILE *A;
    A = fopen(archivo,"wb"); 
    fwrite(buffer, 1, cantidad, A);
    fclose(A);
}

void enmascararConC(char *img1,char *img2, char *mascara,int cant){
    // Comienzo tiempo de ejecucion
    struct timeval begin, end;
    gettimeofday(&begin, 0);

    //Reservar memoria dinámica
    unsigned char *a = malloc(cant);
    unsigned char *b = malloc(cant);
    unsigned char *mask = malloc(cant);

    //Abrimos las imagenes
    abrirArchivo(img1, cant, a);
    abrirArchivo(img2, cant, b);
    abrirArchivo(mascara, cant, mask);

    //Llamo a la funcion en asm
    enmascarar_c(a, b, mask, cant);
    
    //Guardo archivo
    guardarArchivo("salida_c.bmp", cant, a);
    
    //Liberar la memoria
    free(a);
    free(b);
    free(mask);


    //Calculo tiempo de ejecucion
    gettimeofday(&end, 0);
    long microsegundos = end.tv_usec - begin.tv_usec;
  
    printf("Fin ejecucion enmascarar_c: %ld milisegundos.\n", microsegundos/1000);
}

void enmascararConAsm(char *img1,char *img2, char *mascara,int cant){
    // Comienzo tiempo de ejecucion
    struct timeval begin, end;
    gettimeofday(&begin, 0);

    //Reservar memoria dinámica
    unsigned char *a = malloc(cant);
    unsigned char *b = malloc(cant);
    unsigned char *mask = malloc(cant);

    //Abrimos las imagenes
    abrirArchivo(img1, cant, a);
    abrirArchivo(img2, cant, b);
    abrirArchivo(mascara, cant, mask);

    //Llamo a la funcion en asm
    enmascarar_asm(a, b, mask, cant);
    
    //Guardo archivo
    guardarArchivo("salida_asm.bmp", cant, a);
    
    //Liberar la memoria
    free(a);
    free(b);
    free(mask);

    //Calculo tiempo de ejecucion
    gettimeofday(&end, 0);
    long microsegundos = end.tv_usec - begin.tv_usec;
  
    printf("Fin ejecucion enmascarar_asm: %ld milisegundos.\n", microsegundos/1000);

}
   
int main(int argc, char *argv[]){

    //Verifico los argumentos ingresados
    if (argc != 6){
        printf("Error al ingresar los parametros\n");
        return 1;
    }

    //Parametros
    char *img1 = argv[1]; //img1
    char *img2 = argv[2]; //img2
    char *mascara = argv[3]; //mascara
    int ancho = atoi(argv[4]); //paso el argumento "ancho" a int 
    int alto = atoi(argv[5]); //paso el argumento "alto" a int

    //Armo el texto de la resolucion de archivo procesado
    char dimensionesArchivo[10];
    strcat(dimensionesArchivo, argv[4]);
    strcat(dimensionesArchivo, "x");
    strcat(dimensionesArchivo, argv[5]);

    printf("Procesando archivo %s. \n",dimensionesArchivo);
    
    //Calculo cantidad de bytes de las imagenes (las 3 tienen el mismo tamaño)
    int cant = (ancho * alto * 3); //3 bytes por pixel

    //Realizo el enmascarado con ASM   
    enmascararConAsm(img1,img2,mascara,cant);

    //Realizo el enmascarado con C   
    enmascararConC(img1,img2,mascara,cant);

    return 0;
}



